'''@file        closedloop.py
   @brief       Closed loop control used with a motor and encoder to modify and control motor speed
   @details     Takes input values (gain and reference value) to compute actuation levels based on measured values. Can also be used to modify gain value.
   @author      Faith Chau
   @author      Luisa Chiu
   @date        November 15, 2021
'''

class ClosedLoop:
    ''' @brief   A closed loop speed control used in conjuction with the motor and encoder drivers
        @details Objects of this class can be used to control and monitor motor speed
        
    '''

    def __init__ (self, reference, measured, sat_max, sat_min, L, gain):
        ''' @brief            Constructs a controller object that computes an actuation value based on reference and measured values
            @details          This class uses an input reference value, measured value, limits for saturation levels, and an input gain to compute an actuation value.
            @param reference  Variable used to define reference angular velocity used in closed-loop speed control
            @param measured   Variable used to define measured angular velocity used in closed-loop speed control
            @param sat_max    Variable used to define maximum saturation limit for the PWM level used for motor speed
            @param sat_min    Variable used to define minimum saturation limit for the PWM level used for motor speed
            @param L          Variable used to define actuation level
            @param gain       Variable used to define proportional gain value
        '''
        ## @brief    Variable used to define reference angular velocity
        #  @details  This value is the setpoint angular velocity inputted by the user
        self.reference = reference
        ## @brief    Variable used to define measured angular velocity
        #  @details  This value is the angular velocity of the motor, measured by the encoder
        self.measured = measured
        ## @brief    Variable used to define maximum saturation limit for the PWM level
        #  @details  This value is set at 100 to ensure the motor does not exceed 100% PWM level
        self.sat_max = sat_max
        ## @brief    Variable used to define minimum saturation limit for the PWM level
        #  @details  This value is set at -100 to ensure the motor does not fall below -100% PWM level
        self.sat_min = sat_min
        ## @brief    Variable used to define actuation level for motor 1
        #  @details  This value is calculated using gain, measured angular velocity, and reference angular velocity
        self.L = L
        ## @brief    Variable used to define proportional gain value
        #  @details  This value is the proportional gain inputted by the user. Changing this value will tune the plot of angular velocity vs. time.
        self.gain = gain        

    def run (self):
        ''' @brief Uses reference and measured values to define a velocity error, used with gain value, to compute actuation value L
        '''
        ## @brief    Variable used to define the error in speed
        #  @details  This value is the calculated difference between reference velocity and measured velocity.
        self.error = self.reference.read() - self.measured.read()
        self.L.write(self.gain.read()*self.error)
        
        if self.L.read() > self.sat_max:
            self.L.write(self.sat_max)
        if self.L.read() < self.sat_min:
            self.L.write(self.sat_min)
        if self.reference.read() > 0:
            self.L.write(abs(self.gain.read()*self.error))
        if self.reference.read() < 0:
            self.L.write(-abs(self.gain.read()*self.error))
    
    def get__Kp (self):
        ''' @brief Returns gain value for modification
        '''
        return self.Kp
        

    def set__Kp (self, Kp):
        ''' @brief Sets modified gain value
        '''
        ## @brief    Kp represents the proportional gain value
        #  @details  This value is used for modification
        self.Kp = Kp