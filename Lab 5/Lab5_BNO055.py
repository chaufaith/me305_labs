"""
@file     Lab5_BNO055.py
@brief    An orientation sensor driver for the BNO055 from Bosch Sensortec.
@details  Implements a finite state machine that interacts with the ............
@author   Luisa Chiu
@author   Faith Chau
@date     November 9, 2021   
"""
import pyb
import struct
import utime
import os

class BNO055:
    ''' @brief An orientation sensor driver class for the BNO055 from Bosch Sensortec.
        @details Objects of this class can be used to configure the BNO055
                 motor driver .........
    '''
    def __init__ (self, i2c):
        ''' @brief           Constructs an orientation sensor driver object
            @details         ''
            @param i2c       ''
        '''
        ## @brief     ''
        #  @details   ''
        self.i2c = i2c
    
    def set_operating (self):
        ## @brief     ''
        #  @details   ''
        self.i2c.mem_write(0x0C, 0x28, 0x3D)
    
    def get_calib_status (self):
        cal_bytes = self.i2c.mem_read(1, 0x28, 0x35)
        #print("Binary:", '{:#010b}'.format(cal_bytes[0]))
        print('Calibration Status')
        cal_status = (cal_bytes[0] & 0b11,
                     (cal_bytes[0] & 0b11 <<2) >> 2,
                     (cal_bytes[0] & 0b11 <<4) >> 4,
                     (cal_bytes[0] & 0b11 <<6) >> 6)
        print("Values:", cal_status)
        print('\n')        
    
    def get_calib_coef (self): 
        #create byte array of 22 spots, read all 
        #calib_coefs = array.array('b', 22*[0])
        self.calib_coef = self.i2c.mem_read(22, 0x28, 0x55)
        print(self.calib_coef)
        
    def set_calib_coef (self):
        filename = "IMU_cal_coeffs.txt"
        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split()]
        else:
            with open(filename, 'w') as f:
                calib_coef = []
                    
    def euler_angle (self):
        eul_bytes = bytearray(6)
        eul_bytes = self.i2c.mem_read(eul_bytes, 0x28, 0x1A)    
        print('Euler Angles')
        print('Raw bytes:', eul_bytes)
        eul_signed_ints = struct.unpack('<hhh', eul_bytes)
        print('Unpacked: ', eul_signed_ints)        
        eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
        print('Scaled: ', eul_vals)
        print('\n')         
    
    def angular_vel (self):
        ang_bytes = bytearray(6)
        ang_bytes = self.i2c.mem_read(ang_bytes, 0x28, 0x14)
    
        print('Angular Velocity')
        print('Raw bytes:', ang_bytes)
        ang_signed_ints = struct.unpack('<hhh', ang_bytes)
        print('Unpacked: ', ang_signed_ints)
        
        ang_vals = tuple(ang_int/900 for ang_int in ang_signed_ints)
        print('Scaled: ', ang_vals)
        print('\n')  
        
    
if __name__ == '__main__':
       i2c = pyb.I2C(1, pyb.I2C.MASTER) 
       driver = BNO055(i2c)
       driver.set_operating()
       while True:
           driver.get_calib_status()
           driver.get_calib_coef()
           #driver.euler_angle()
           #driver.angular_vel()
           utime.sleep(0.5)
           
