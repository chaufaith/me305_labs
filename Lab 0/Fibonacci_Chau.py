# -*- coding: utf-8 -*-
"""
Created on Tue Sep 21 15:26:33 2021

@author: Faith Chau
"""
# Import math - use for math eqn method

storage = {} # Storage is where we will store all the previously calculated fib numbers
def fib(idx): # Defining our Fibonacci function fib
    if int(idx) == 0:   # Inputting givens into the function.
        return 0        # fib(0) = 0, fib(1) = 1
    elif int(idx) == 1:
        return 1
    if idx in storage: # If we have already calculated fib(idx), return it from storage to use in remaining fib calculation
        return storage[idx]
    else: # If we haven't already calculatd fib(idx), use the following sequence to calculate
        storage[idx] = fib((idx)-1) + fib((idx)-2) # Store calculated value back into storage
        return storage[idx] # Return final value of Fibonacci sequence
    
# Use for mathematical equation method: return math.trunc(1/math.sqrt(5)*((1+math.sqrt(5))/2)**(int(idx)) - 1/math.sqrt(5)*((1-math.sqrt(5))/2)**(int(idx)))
    
if __name__ == '__main__': # Lets us test our function when script is run as a standalone program
    
    while (True): # Creates a loop that goes on forever, until we exit
        my_string = input('Input index: ')
        try: # Testing index validity by seeing if the input is an integer
            idx = int(my_string) # int() returns an integer from my_string input
        except: # If 'try' code doesn't work, 'except' function runs and prompts user to input a valid index
            print ('Index must be an integer!')
            continue # Takes us back to top of while loop
        if int(my_string) < 0: # Testing index validity by seeing if the input is a positive number; if index is <0, user is prompted to input a valid index
            print ('Index must be a nonnegative number!')
        else: # If index is valid and satisfies 'try' code, Fibonacci function can run and the output is given
            print('Fibonacci number at '
                          'index {:} is {:}. '.format(idx,fib(idx)))
            prompt = input('Press Q to quit or press any other key to continue:') # Prompting user to quit or continue
            if prompt == 'Q': # If 'Q' is pressed, break out of the loop
                break  # Stops running loop

    

            
             
    
             
            
        
           
            
                
               
           
                

    



